using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest : MonoBehaviour
{
    [SerializeField] private Text FirstButtonText;
    [SerializeField] private Text SecondButtonText;
    [SerializeField] private Text ThirdButtonText;
    [SerializeField] private GameObject DialogScreen;
    [SerializeField] private Text QuestText;
    [SerializeField] private int LoadSceneIndex;
    private LevelLoader levelLoader;
    private int currentQuestIndex;

    private void Start()
    {
        levelLoader = FindObjectOfType<LevelLoader>();
    }

    public void ChooseQuest(int indexQuest)
    {
        currentQuestIndex = indexQuest;
        var currentQuest = QuestManager.Instance.GetQuestByNumber(indexQuest);
        FirstButtonText.text = currentQuest.FirstButtonDescription;
        SecondButtonText.text = currentQuest.SecondButtonDescription;
        ThirdButtonText.text = currentQuest.thirdButtonDescription_Action;
        QuestText.text = currentQuest.QuestDescription;
    }

    public void FirstButton()
    {
        QuestText.text = QuestManager.Instance.GetQuestByNumber(currentQuestIndex).FirstButtonAnswer;
    }

    public void SecondButton()
    {
        QuestText.text = QuestManager.Instance.GetQuestByNumber(currentQuestIndex).SecondButtonAnswer;
    }

    public void ThirdButtonAction()
    {
        var questIndex = (QuestNumber)PlayerPrefs.GetInt("NumberQuest");
        DialogScreen.SetActive(false);
        if (questIndex == QuestNumber.FifthQuest)
        {
            Player.Instance.DeleteDemonHeadFromStatusBar();
            PlayerPrefs.DeleteKey("DemonHead");
            PlayerPrefs.SetInt("Crystal", 1);
            Player.Instance.AddArtefactCrystalOnStatusBar();
        }

        if (questIndex == QuestNumber.SixthQuest)
        {
            levelLoader.LoadLevel(LoadSceneIndex);
            return;
        }
        currentQuestIndex++;
        PlayerPrefs.SetInt("NumberQuest", currentQuestIndex);
    }
}
