using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New QuestData", menuName = "QuestData", order = 51)]
public class QuestData : ScriptableObject
{
    [SerializeField] private int NumberQuest;
    [TextArea(10, 10)]
    [SerializeField] private string DescriptionQuest;
    [SerializeField] private string TextOfButtonFirst;
    [TextArea(10, 10)]
    [SerializeField] private string AnswerOfFirstButton;
    [SerializeField] private string TextOfButtonSecond;
    [TextArea(10, 10)]
    [SerializeField] private string AnswerOfSecondButton;
    [SerializeField] private string TextOfButtonThird_Action;

    public int QuestNumber => NumberQuest;
    public string QuestDescription => DescriptionQuest;
    public string FirstButtonDescription => TextOfButtonFirst;
    public string FirstButtonAnswer => AnswerOfFirstButton;
    public string SecondButtonDescription => TextOfButtonSecond;
    public string SecondButtonAnswer => AnswerOfSecondButton;
    public string thirdButtonDescription_Action => TextOfButtonThird_Action;

}
