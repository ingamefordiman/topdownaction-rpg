using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    [SerializeField] private InteractableObjects focus;
    [SerializeField] private float agrRadius = 5f;
    private bool die = false;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, agrRadius);
    }

    protected override void Update()
    {
        if (!die)
        {
            base.Update();
            float distance = Vector3.Distance(transform.position, Player.Instance.transform.position);
            if (distance <= agrRadius)
            {
                if (focus == null)
                    SetFocus(Player.Instance.GetComponent<InteractableObjects>());
            }
            else if (focus != null)
            {
                DeleteFocus();
            }
        }
    }

    public void SetFocus(InteractableObjects newFocus)
    {
        if (newFocus != focus)
        {
            if (focus != null)
                focus.OnDeFocus();
        }
        focus = newFocus;
        motor.FollowToTarget(newFocus);
        newFocus.OnFocus(gameObject);
    }

    public void DeleteFocus()
    {
        if (focus != null)
            focus.OnDeFocus();
        focus = null;
        motor.StopFollowing();
    }

    protected override void Die()
    {
        die = true;
        DieEffect.SetActive(true);
        Player.Instance.DeleteFocus();
        GetComponent<Enemy>().enabled = false;
        DeleteFocus();
        motor.Die();
        Player.Instance.AddCoins(Random.Range(20, 30));
    }
}
