using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Player : Character
{
    [SerializeField] private LayerMask CanMove;
    [SerializeField] private InteractableObjects focus;
    [SerializeField] private Slider sliderHealth;
    [SerializeField] private Image HealPotion;
    [SerializeField] private Button HealButton;
    [SerializeField] private int coinsCount;
    [SerializeField] private Text CoinsText;
    [SerializeField] private GameObject SpawnPosition;
    [SerializeField] private GameObject DialogScreen;
    [SerializeField] private GameObject HealEffect;
    [SerializeField] private Image NoDemonHead;
    [SerializeField] private Sprite DemonHead;
    [SerializeField] private Image NoCrystalArtefact;
    [SerializeField] private Sprite CrystalArtefact;
    [SerializeField] private Sprite Commonbackground;
    [SerializeField] private GameObject DieScreen;
    private Camera CameraMain;
    private bool die = false;
    private bool isHeal = false;

    public static Player Instance;

    private void Awake()
    {
        Instance = this;
        CameraMain = Camera.main;
        coinsCount = PlayerPrefs.GetInt("Coins");
        transform.position = new Vector3(SpawnPosition.transform.position.x, SpawnPosition.transform.position.y, SpawnPosition.transform.position.z);
        CoinsText.text = coinsCount.ToString();
        if (PlayerPrefs.HasKey("DemonHead"))
            AddDemonHeadOnStatusBar();
        if (PlayerPrefs.HasKey("Crystal"))
            AddArtefactCrystalOnStatusBar();
    }

    protected override void Update()
    {
        if (!die)
        {
            base.Update();
            sliderHealth.value = currentHealth;
            sliderHealth.maxValue = maxHealth;
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = CameraMain.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray))
                {
                    if (EventSystem.current.IsPointerOverGameObject())
                        return;
                }

                if (Physics.Raycast(ray, out hit, 100, CanMove))
                {
                    motor.MoveToPoint(hit.point);
                    DeleteFocus();
                }
            }

            else if (Input.GetMouseButtonDown(1))
            {
                Ray ray = CameraMain.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100))
                {
                    var interactable = hit.collider.GetComponent<InteractableObjects>();
                    if (interactable != null)
                    {
                        SetFocus(interactable);
                    }
                }
            }
        }
    }

    public void SetFocus(InteractableObjects newFocus)
    {
        if(newFocus != focus)
        {
            if (focus != null)
                focus.OnDeFocus();
        }
        focus = newFocus;
        motor.FollowToTarget(newFocus);
        newFocus.OnFocus(gameObject);
    }

    public void DeleteFocus()
    {
        if (focus != null)
            focus.OnDeFocus();
        focus = null;
        motor.StopFollowing();
    }

    public void Healing()
    {
        if (currentHealth >= 100)
            print("Максимальное здоровье");
        else
        {
            if ((currentHealth += 10) >= 100 && !isHeal)
                currentHealth = maxHealth;
            else if (!isHeal)
            {
                currentHealth += 20f;
                StartCoroutine(EffectHeal());
            }
        }
    }

    private IEnumerator EffectHeal()
    {
        HealPotion.color = new Color32(100, 100, 100, 100);
        HealButton.interactable = false;
        isHeal = true;
        HealEffect.SetActive(true);
        yield return new WaitForSeconds(3f);
        HealEffect.SetActive(false);
        yield return new WaitForSeconds(2f);
        isHeal = false;
        HealButton.interactable = true;
        HealPotion.color = new Color32(255, 255, 255, 255);
    }

    protected override void Die()
    {
        Player.Instance.enabled = false;
        DieEffect.SetActive(true);
        die = true;
        DeleteFocus();
        motor.Die();
        OnDie();
        StartCoroutine(DieTime());
    }

    private IEnumerator DieTime()
    {
        yield return new WaitForSeconds(3f);
        DieScreen.SetActive(true);
    }

    public void DialogsOnPLayer()
    {
        quest.ChooseQuest(PlayerPrefs.GetInt("NumberQuest"));
        DialogScreen.SetActive(true);
    }

    public void AddCoins(int coins)
    {
        coinsCount += coins;
        PlayerPrefs.SetInt("Coins", coinsCount);
        CoinsText.text = coinsCount.ToString();
    }

    public void AddDemonHeadOnStatusBar()
    {
        NoDemonHead.sprite = DemonHead;
    }

    public void DeleteDemonHeadFromStatusBar()
    {
        NoDemonHead.sprite = Commonbackground;
    }

    public void AddArtefactCrystalOnStatusBar()
    {
        NoCrystalArtefact.sprite = CrystalArtefact;
    }
}
