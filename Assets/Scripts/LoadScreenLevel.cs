using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadScreenLevel : MonoBehaviour
{
    [SerializeField] private GameObject LoadCaveScreen;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
            LoadCaveScreen.SetActive(true);
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
            LoadCaveScreen.SetActive(false);
    }
}
