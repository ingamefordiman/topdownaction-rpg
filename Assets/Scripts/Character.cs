using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AgentMotor))]
public abstract class Character : InteractableObjects
{
    public override void Interact(GameObject subject)
    {
        StartCoroutine(OnInteract(subject));
    }

    [SerializeField] protected float maxHealth = 100f;
    [SerializeField] protected float currentHealth;
    [SerializeField] private float damage = 5f;
    [SerializeField] private float attackCD = 1f;
    [SerializeField] private bool canAttack = true;
    [SerializeField] protected GameObject DieEffect;
    [SerializeField] protected ParticleSystem HitEffect_1;
    [SerializeField] protected ParticleSystem HitEffect_2;
    [SerializeField] protected ParticleSystem HitEffect_3;
    protected AgentMotor motor;
    protected Quest quest;

    private void Start()
    {
        motor = GetComponent<AgentMotor>();
        currentHealth = maxHealth;
        quest = GetComponent<Quest>();
    }

    private IEnumerator OnInteract(GameObject subject)
    {
        var character = subject.GetComponent<Character>();
        if (character != null)
            if (character.canAttack)
            {
                while(isFocus && subject != null)
                {
                    if(Vector3.Distance(transform.position, subject.transform.position) <= InteractRadius)
                    {
                        character.Attack();
                        yield return new WaitForSeconds(0.7f);
                        TakeDamage(character.damage);
                        HitEffect_1.Play();
                        HitEffect_2.Play();
                        HitEffect_3.Play();
                        yield return new WaitForSeconds(character.attackCD);
                    }
                    yield return null;
                }
            }
    }

    private void TakeDamage(float damage)
    {
        currentHealth -= damage;
        print($"�������� ��������� {gameObject.name}: {currentHealth}");
        if(currentHealth <= 0)
        {
            Die();
        }
    }

    public void Attack()
    {
        motor.StartAttack(attackCD);
    }

    protected abstract void Die();
}
