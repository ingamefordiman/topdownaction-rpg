using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : InteractableObjects
{
    public override void Interact(GameObject subject)
    {
        Player.Instance.Healing();
    }
}
