using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private AudioSource soundsVolume, musicVolume;
    [SerializeField] private Slider soundSlider;
    [SerializeField] private Button buttonContinue;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("soundVolume"))
            PlayerPrefs.SetFloat("soundVolume", 7f);
        if (!PlayerPrefs.HasKey("musicSound"))
            PlayerPrefs.SetFloat("musicSound", 7f);

        soundSlider.value = PlayerPrefs.GetFloat("soundVolume");

        CheckingButton();
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("soundVolume", soundSlider.value);
        PlayerPrefs.SetFloat("musicSound", soundSlider.value);
        soundsVolume.volume = PlayerPrefs.GetFloat("soundVolume");
        musicVolume.volume = PlayerPrefs.GetFloat("musicSound");
    }

    private void CheckingButton()
    {
        if (!PlayerPrefs.HasKey("BeginGame"))
        {
            print("���������� �� �������");
            buttonContinue.interactable = false;
        }
        else if (PlayerPrefs.HasKey("BeginGame"))
        {
            print("���������� �������");
            buttonContinue.interactable = true;
        }
    }

    public void NewGame()
    {
        PlayerPrefs.SetInt("BeginGame", 0);
    }

    public void DeleteSaves()
    {
        PlayerPrefs.DeleteAll();
        CheckingButton();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
