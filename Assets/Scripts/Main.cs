using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    [SerializeField] private GameObject PauseScreen;
    [SerializeField] private AudioSource soundSource, musicSource;
    [SerializeField] private Slider soundSlider;
    private SoundsEffector soundsEffector;

    private void Start()
    {
        soundSource.volume = PlayerPrefs.GetFloat("soundVolume");
        musicSource.volume = PlayerPrefs.GetFloat("musicSound");
        soundSlider.value = PlayerPrefs.GetFloat("soundVolume");
        soundsEffector = GetComponent<SoundsEffector>();
    }

    private void Update()
    {
        PlayerPrefs.SetFloat("soundVolume", soundSlider.value);
        PlayerPrefs.SetFloat("musicSound", soundSlider.value);
        soundSource.volume = PlayerPrefs.GetFloat("soundVolume");
        musicSource.volume = PlayerPrefs.GetFloat("musicSound");

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            soundsEffector.PlayInventorySound();
            PauseOn();
        }
    }

    public void PauseOn()
    {
        Time.timeScale = 0f;
        Player.Instance.enabled = false;
        PauseScreen.SetActive(true);
    }

    public void PauseOff()
    {
        Time.timeScale = 1f;
        Player.Instance.enabled = true;
        PauseScreen.SetActive(false);
    }
    public void AddPlayerHealth()
    {
        Player.Instance.Healing();
    }

    public void FirstQuest()
    {
        PlayerPrefs.SetInt("NumberQuest", 0);
    }

    public void SecondQuest()
    {
        PlayerPrefs.SetInt("NumberQuest", 1);
    }

    public void thirdQuest()
    {
        PlayerPrefs.SetInt("NumberQuest", 2);
    }

    public void fourthQuest()
    {
        PlayerPrefs.SetInt("NumberQuest", 3);
    }

    public void fiveQuest()
    {
        PlayerPrefs.SetInt("NumberQuest", 4);
    }
    public void sixQuest()
    {
        PlayerPrefs.SetInt("NumberQuest", 5);
    }
}
