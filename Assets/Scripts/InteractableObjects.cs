using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObjects : MonoBehaviour
{
    public float InteractRadius = 2f;
    protected bool isFocus = false;
    protected GameObject subject;
    private bool hasInteract = false;

    public abstract void Interact(GameObject subject);

    protected virtual void Update()
    {
        if(isFocus && !hasInteract)
        {
            float distance = Vector3.Distance(transform.position, subject.transform.position);
            if(distance <= InteractRadius)
            {
                hasInteract = true;
                Interact(subject);
            }
        }
    }

    public void OnFocus(GameObject newSubject)
    {
        isFocus = true;
        subject = newSubject;
        hasInteract = false;
    }

    public void OnDeFocus()
    {
        isFocus = false;
        subject = null;
        hasInteract = false;
    }

    public void OnDie()
    {
        OnDeFocus();
    }
}
